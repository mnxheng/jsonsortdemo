import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import org.apache.commons.codec.digest.DigestUtils;

public class JsonSortDemo {

    public static void main(String[] args) {
        String request = "{\"job\":\"火枪手\",\"contacts\":[{\"name\":\"宙斯\",\"relation\":\"朋友\"},{\"name\":\"奥丁\",\"relation\":\"同学\"}],\"manager\":{\"name\":\"拉斐尔\",\"age\":31}}";
        String secretToken = "";
        JSONObject jsonObject = JSON.parseObject(request, Feature.OrderedField);
        String signRequest = signRequest(jsonSortByKey(jsonObject), secretToken);
        System.out.println(signRequest);

        // Print string: contactsname宙斯relation朋友name奥丁relation同学job火枪手managerage31name拉斐尔
        // MD5: c734a494d015e55c6bc6b4ba6a53d3a8
    }

    /**
     * json按key排序
     *
     * @param jsonObject
     * @return
     */
    private static JSONObject jsonSortByKey(JSONObject jsonObject) {
        if (null == jsonObject) {
            return null;
        }
        // 使用LinkedHashMap
        JSONObject jsonObjectOrder = new JSONObject(true);
        // 递归排序
        jsonObject.keySet().stream().sorted().forEach(key -> {
                    // 排除字符串数组属于JSONArray的情况
                    if (jsonObject.get(key) instanceof JSONArray && (((JSONArray)jsonObject.get(key)).get(0) instanceof JSONObject)) {
                        for (int i = 0; i < ((JSONArray) jsonObject.get(key)).size(); i++) {
                            ((JSONArray) jsonObject.get(key)).add(i, jsonSortByKey(((JSONArray) jsonObject.get(key)).getJSONObject(i)));
                            ((JSONArray) jsonObject.get(key)).remove(i + 1);
                        }
                    }
                    if (jsonObject.get(key) instanceof JSONObject) {
                        jsonObject.fluentPut(key, jsonSortByKey((JSONObject) jsonObject.get(key)));
                    }
                    jsonObjectOrder.fluentPut(key, jsonObject.get(key));
                }
        );
        return jsonObjectOrder;
    }

    /**
     * 加签，post请求参数
     *
     * @param jsonObject
     * @param secretToken
     * @return
     */
    private static String signRequest(JSONObject jsonObject, String secretToken) {
        StringBuffer sb = new StringBuffer();
        // 如果请求json为null，当做空字符串处理
        if (null == jsonObject) {
            return DigestUtils.md5Hex(sb.append(secretToken).append(secretToken).toString());
        }
        jsonObject.keySet().stream().forEach(key ->
                sb.append(key).append(jsonObject.get(key).toString())
        );
        String regEx = "[()={}:;,\\[\\]\\s\"]";
        String handleRequest = sb.toString().replaceAll(regEx, "");
        System.out.println(handleRequest);
        System.out.println(DigestUtils.md5Hex(handleRequest));
        return DigestUtils.md5Hex(secretToken + handleRequest + secretToken);
    }
}
